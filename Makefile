subdir-m += GobiNet
subdir-m += GobiSerial

all: clean
	$(MAKE) -C GobiNet
	$(MAKE) -C GobiSerial
install: all
	$(MAKE) -C GobiNet install
	$(MAKE) -C GobiSerial install
	depmod
clean:
	$(MAKE) -C GobiNet clean
	$(MAKE) -C GobiSerial clean

